<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Enable the cookies",
	'L_CC_DISMISS'					=> "Accept",
	'L_CC_LINK'						=> "Learn more",
	'L_CC_MESSAGE'					=> "This website uses cookies to ensure you get the best experience on our website",
	'L_CHAPO'						=> "Filter the articles with header",
	'L_CHAPO_HINT'					=> "For homepage, categories and tags pages, don' display social-networks  buttons if  the article has a header.",
	'L_COOKIE'						=> "Ask for cookies",
	'L_COOKIE_POLICY'				=> "Static page for the cookies policy",
	'L_COOKIE_POLICY_NO'			=> "No",
	'L_DRAG_AND_DROP'				=> "Sort the networks by moving the icons above",
	'L_IMAGE_INFO'					=> "About the size of the images",
	'L_MEDIA'						=> "Default image",
	'L_MEDIA_TITLE'					=> "Browse the medias folder",
	'L_OGP_DEBUGGER'				=> "Open Graph degugger by Facebook",
	'L_OPENGRAPH'					=> "Learn more about the Open Graph protocole",
	'L_SAVE'						=> "Save",
	'L_TAGS'						=> "Share the tags"
);
?>
