<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Aktivieren Sie die Cookies",
	'L_CC_DISMISS'					=> "Akzeptieren",
	'L_CC_LINK'						=> "Mehr erfahren",
	'L_CC_MESSAGE'					=> "Diese Website verwendet Cookies, um sicherzustellen, dass Sie das beste Erlebnis auf unserer Website erhalten",
	'L_CHAPO'						=> "Artikel mit Kopfzeile filtern",
	'L_CHAPO_HINT'					=> "Zeigen Sie für Homepage-, Kategorien- und Tagseiten keine Schaltflächen für soziale Netzwerke an, wenn der Artikel eine Kopfzeile enthält.",
	'L_COOKIE'						=> "Fragen Sie nach Keksen",
	'L_COOKIE_POLICY'				=> "Statische Seite für die Cookie-Richtlinie",
	'L_COOKIE_POLICY_NO'			=> "Nein",
	'L_DRAG_AND_DROP'				=> "Sortieren Sie die Netzwerke, indem Sie die Symbole oben verschieben",
	'L_IMAGE_INFO'					=> "Über die Größe der Bilder",
	'L_MEDIA'						=> "Standardbild",
	'L_MEDIA_TITLE'					=> "Durchsuchen Sie den Medienordner",
	'L_OGP_DEBUGGER'				=> "Öffnen Sie den Graph-Debugger von Facebook",
	'L_OPENGRAPH'					=> "Erfahren Sie mehr über das Open Graph-Protokoll",
	'L_SAVE'						=> "sparen",
	'L_TAGS'						=> "Teilen Sie die Tags"
);
?>
